package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bidorbuy1Application {

	public static void main(String[] args) {
		SpringApplication.run(Bidorbuy1Application.class, args);
	}

}

