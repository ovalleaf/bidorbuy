package com.example.demo.services;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

import com.example.demo.model.SearchResponse;

@Component
public class BidorBuyServices {
	
	private Logger log = Logger.getLogger(BidorBuyServices.class);

	public SearchResponse searchTrade(HttpURLConnection httpUrlconnnection) throws IOException {
		log.info("searchTrade....");
		return (SearchResponse) httpUrlconnnection.getContent();
	}
}
