package com.example.demo.model;

public class Images {
	private String image;
	private String thumbnail;

	public Images() {
		this.image = null;
		this.thumbnail = null;
	}

	public Images(String image, String thumbnail) {
		this.image = image;
		this.thumbnail = thumbnail;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
