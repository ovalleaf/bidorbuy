package com.example.demo.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class SearchResponse {
	private int totalResults;
	private int pageNumber;
	private int resultsPerPage;
	private ArrayList<Images> trade;
    private Float amount;
    private String title;
    private String type;
    private Integer userId;
    private Boolean hotSelling;
    private String categoryBreadCrumb;
    private String userAlias;
    private LocalDateTime closeTime;
    private Integer homeCategoryId;
    private String location;
    private LocalDateTime openTime;
    private Integer tradeId;
    private String status;
   
    
    
	public int getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getTradeId() {
		return tradeId;
	}
	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}
	public LocalDateTime getOpenTime() {
		return openTime;
	}
	public void setOpenTime(LocalDateTime openTime) {
		this.openTime = openTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getHomeCategoryId() {
		return homeCategoryId;
	}
	public void setHomeCategoryId(Integer homeCategoryId) {
		this.homeCategoryId = homeCategoryId;
	}
	public LocalDateTime getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(LocalDateTime closeTime) {
		this.closeTime = closeTime;
	}
	public String getUserAlias() {
		return userAlias;
	}
	public void setUserAlias(String userAlias) {
		this.userAlias = userAlias;
	}
	public String getCategoryBreadCrumb() {
		return categoryBreadCrumb;
	}
	public void setCategoryBreadCrumb(String categoryBreadCrumb) {
		this.categoryBreadCrumb = categoryBreadCrumb;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Boolean getHotSelling() {
		return hotSelling;
	}
	public void setHotSelling(Boolean hotSelling) {
		this.hotSelling = hotSelling;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public ArrayList<Images> getTrade() {
		return trade;
	}
	public void setTrade(ArrayList<Images> trade) {
		this.trade = trade;
	}
    
  

	

}
