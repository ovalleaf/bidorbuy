package com.example.demo.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.SearchResponse;
import com.example.demo.services.BidorBuyServices;

@Controller
public class BidorBuyController {
	
	private static final String X_BOB_AUTHID = "kfpP9jzHLmoTqRBtzGvxkYYF2GzfWfWhtgHGZVpB";
	private static final String X_BOB_PLATFORM = "4";
	private static final String X_BOB_CID = "987654321";
	
	private static final String GET_URL = "https://demo.bidorbuy.co.za/services/v3/tradesearch?";

	
	@Autowired
	private BidorBuyServices bidorbuyService;
	
	@GetMapping("api/search")
	@ResponseBody
	public SearchResponse tradeSearch() {
		
		URL url;
		try {
			url = new URL(GET_URL);
			HttpURLConnection httpUrlconnnection = (HttpURLConnection) url.openConnection();
			httpUrlconnnection.setRequestMethod("GET");
			// adding headers
			httpUrlconnnection.setRequestProperty("X-BOB-AUTHID", X_BOB_AUTHID);
			httpUrlconnnection.setRequestProperty("X-BOB-PLATFORM", X_BOB_PLATFORM);
			httpUrlconnnection.setRequestProperty("X-BOB-CID", X_BOB_CID);
		
			return bidorbuyService.searchTrade(httpUrlconnnection);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

		
	}


}
